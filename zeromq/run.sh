#!/bin/bash

export HOME=/home/user

/usr/local/bin/gosu root /bin/chmod 750 /var/run/httpd/
/usr/local/bin/gosu root /bin/chmod 750 /run/httpd/
/usr/local/bin/gosu root /bin/rm -rf /var/run/httpd/* /run/httpd/* /tmp/httpd*

#/usr/local/bin/gosu root /sbin/httpd -D FOREGROUND
/usr/local/bin/gosu root /usr/bin/supervisord -c /etc/supervisord.d/supervisord.conf -n 
#/usr/local/bin/gosu root /usr/bin/redis-server
